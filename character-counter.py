#!/usr/bin/env python

# #############################################################################
# Name:        Character Counter
# #############################################################################
__author__ = 'jolo'
__version__ = '0.1.0'
__date__ = '2016-05-10'
__help__ = """Character Counter

This script counts the characters of all files with a given file extension
within a given directory or of a single file. It plots a histogram of the character distribution.
"""


# #############################################################################
# IMPORTS
# #############################################################################
import argparse
import os
import pylab as pl
import numpy as np
import json
from binaryornot.check import is_binary


# #############################################################################
# SUBROUTINES
# #############################################################################
def printStatistics(fileCounter, sortedChars, ngram):
    """
    Print some statistics
    :param fileCounter:
    :param sortedChars:
    :param ngram: width of n-gram
    """

    totalChars = 0
    for char in sortedChars:
        totalChars = totalChars + char[1]

    totalIndividualChars = len(sortedChars)

    print("##########################################################")
    print("Statistics")
    print("  Total files processed:      "+str(fileCounter))
    print("  Width of n-grams:           "+str(ngram))
    print("  Total n-grams counted:      "+str(totalChars))
    print("  Total individual n-grams:   "+str(totalIndividualChars))
    print("  Top ten n-grams in descending order:")

    topTenCharsCount = 0
    for char in sortedChars[0:9]:
        print("    '"+str(char[0])+"': "+str(char[1]))
        topTenCharsCount = topTenCharsCount + char[1]

    print("  These top ten account for {:.1f}% of all n-grams".format(100*topTenCharsCount/totalChars))

    return


def replaceKey(dict, oldKey, newKey):
    """
    Replace the key character 'oldkey' with 'newKey'. Does not create newKey if oldKey does not exist in the first place.

    :param dict: character dictionary
    :param oldKey: old key name/char
    :param newKey: new key name/char
    :return: new character dictionary with 'newKey' instead of 'oldKey'
    """
    if oldKey in dict:
        dict = addDict(dict, {newKey: dict[oldKey]})
        dict.pop(oldKey, None)

    return dict


def countUpperCaseAsShift(dict):
    """
    Count upper case letters as SHIFT+lower case
    :param dict: character dictionary
    :return: new character dictionary with separate shift and lower case letter counts
    """
    for key, val in dict.items():
        if len(key) == 1 and key.isupper():
            dict = addDict(dict, {'SHIFT': val})
            dict = addDict(dict, {key.lower(): val})
            dict.pop(key, None)

    return dict


def removeEolChars(dict):
    """
    Remove all known eol codings (Line feed, Carriage return, or both)
    :param dict: character dictionary
    :return: new character dictionary without EOL chars
    """
    dict.pop('\n', None)
    dict.pop('\r', None)

    return dict


def plotDictHist(dict):
    """
    Show a histogram plot of the character distribution as defined by the
    given dict.
    :param dict: dictionary with keys (character) and values (frequency)
    """
    X = np.arange(len(dict))
    pl.bar(X, dict.values(), align='center', width=0.5)
    pl.xticks(X, dict.keys())
    ymax = max(dict.values()) + 1
    pl.ylim(0, ymax)
    pl.show()


def plotListHist(list):
    """
    Show a histogram plot of the character distribution as defined by the
    given list.
    :param list: list(dict)
    """
    X = np.arange(len(list))
    pl.bar(X, list.values(), align='center', width=0.5)
    pl.xticks(X, list.keys())
    ymax = max(list.values()) + 1
    pl.ylim(0, ymax)
    pl.show()


def addDict(dict1, dict2):
    """
    add the values of dict1.key to the values of dict2.key
    :param dict1: dictionary with keys and integer values
    :param dict2: dictionary with keys and integer values
    :return: dictionary with added values of dict1 and dict2
    """
    for key, val in dict1.items():
        if key in dict2:
            dict2[key] = dict2[key] + dict1[key]
        else:
            dict2[key] = dict1[key]

    return dict2


def countChars(str, ngram):
    """
    count all chars (or n-grams) in the given string
    :param str: string
    :param ngram: width of char combinations, like 2 for bigrams, 3 for trigrams
    :return: dictionary = {"each found char": "number of occurrences"}
    """
    chars = dict()

    i = 0
    while i+ngram <= len(str):
        chars = addDict(chars, dict({str[i:i+ngram]: 1}))
        i = i + 1

    return chars


def countCharsInFile(file, ngram):
    """
    count all chars (or n-grams) in the given file
    :param file: file path
    :param ngram: width of char combinations, like 2 for bigrams, 3 for trigrams
    :return: dictionary = {"each found char": "number of occurrences"}
    """
    chars = dict()

    with open(file) as f:
        i = 0

        try:
            for i, l in enumerate(f):
                chars = addDict(chars, countChars(l, ngram))
                pass

            chars = addDict(chars, dict({'ENTER': i}))

        except UnicodeDecodeError:
            print("Unicode decoding failed for "+file)

    return chars



# #############################################################################
# MAIN
# #############################################################################
def main(args):
    chars = dict()

    fileCounter = 0
    if os.path.isdir(args.directory):
        for root, subFolders, files in os.walk(args.directory):
            if args.include is not None:
                files = [file for file in files if file.endswith(tuple(args.include))]
            if args.exclude is not None:
                files = [file for file in files if not file.endswith(tuple(args.exclude))]
            if args.nobinaries is not None:
                files = [file for file in files if not is_binary(os.path.join(root, file))]
            for file in files:
                fileCounter = fileCounter + 1
                filePath = os.path.join(root, file)
                chars = addDict(chars, countCharsInFile(filePath, args.ngram))
    else:
        fileCounter = 1
        chars = addDict(chars, countCharsInFile(args.directory, args.ngram))

    if args.shift:
        chars = countUpperCaseAsShift(chars)

    if args.eol:
        del chars['ENTER']
    else:
        chars = removeEolChars(chars)

    if args.whitespace:
        chars = replaceKey(chars, ' ', 'SPACE')

    if args.tab:
        chars = replaceKey(chars, '\t', 'TAB')

    if not chars:
        print("No data found")
        return

    from operator import itemgetter
    sortedChars = sorted(chars.items(), key=itemgetter(1), reverse=True)

    if args.verbose:
        printStatistics(fileCounter, sortedChars, args.ngram)

    if args.plot:
        plotDictHist(chars)

    if args.output:
        outFile = open(args.output, 'w')
        if args.adnw_optimized:
            for char in sortedChars:
                print(str(char[1])+" "+char[0], file=outFile)
        else:
            json.dump(sortedChars, outFile, indent=4, separators=(',', ': '))

        outFile.close()

    else:
        print(sortedChars)

    return


# #############################################################################
# MAIN
# #############################################################################
if __name__ == '__main__':
    # #########################################################################
    # ## command line parsing (-h/--help and -v/--version is always supported)
    # #########################################################################
    parser = argparse.ArgumentParser(description=__help__, epilog='', formatter_class=argparse.RawTextHelpFormatter)
    # help argument is supported by python itself
    parser.add_argument('-v', '--version', action='version', version='%(prog)s ' + __version__)
    parser.add_argument('directory', default=True, help='Search directory or file')
    parser.add_argument('-i', '--include', default=None, action='append', dest='include', help='Include only filenames ending with the given pattern')
    parser.add_argument('-e', '--exclude', default=None, action='append', dest='exclude', help='Exclude each filename ending with the given pattern')
    parser.add_argument('-s', '--shift', default=False, action='store_true', help='Count uppercase letters as SHIFT + lower case letter separately')
    parser.add_argument('-l', '--eol', default=False, action='store_true', help='Count all end of line chars separately instead of a combined ENTER')
    parser.add_argument('-w', '--whitespace', default=False, action='store_true', help='State a whitespace character as SPACE')
    parser.add_argument('-t', '--tab', default=False, action='store_true', help='State a tab character as TAB')
    parser.add_argument('-p', '--plot', default=False, action='store_true', help='Show a histogram plot')
    parser.add_argument('-vv', '--verbose', default=False, action='store_true', help='Verbose output, served with some statistics ')
    parser.add_argument('-o', '--output', default=False, action='store', help='Print resulting char list json encoded to given output file rather than stdout')
    parser.add_argument('-b', '--nobinaries', default=False, action='store_true', help='Try ignoring binary files')
    parser.add_argument('-n', '--ngram', type=int, default=1, action='store', help='Define number of chars to count n-grams instead of single letter chars')
    parser.add_argument('-a', '--adnw_optimized', default=False, action='store_true', help='Output files use the format for the ADNW optimizer')

    args = parser.parse_args()


    # #########################################################################
    # ## start main function
    # #########################################################################
    try:
        main(args)

    # 1. our own exceptions are of type RuntimeError
    # 2. Any other type of exception is really bad and is not caught
    except RuntimeError as rerr:
        # 3. we do not delete any created temporary files and folders on purpose,
        # because to investigate the exception cause, 
        print("\nOops,")
        print("there is a runtime error, investigate the problem and clean ")
        print("all created temporary files and folders manually!\n")

        raise rerr
